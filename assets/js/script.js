// canvas setup
const canvas = document.getElementById('the_canvas');
const context = canvas.getContext('2d');

canvas.width = 640;
canvas.height = 800;

// player and enemy settings
const PLAYER_WIDTH = 117;
const ENEMY2_WIDTH = 115;
const PLAYER_HEIGHT = 110;
const healthRegeneration = 20;
let isGameOver = false;
let enemy1HitCount = 0;
let enemy2HitCount = 0;

// define images
let background = new Image();
let playerImage = new Image();
let enemy1Image = new Image();
let enemy2Image = new Image();
let explosionImage = new Image();
let bulletImage = new Image();
let powerupImage = new Image();
background.src = "assets/img/background.jpg";
playerImage.src = "assets/img/planes.png";
enemy1Image.src = "assets/img/planes.png";
enemy2Image.src = "assets/img/planes.png";
explosionImage.src = "assets/img/explosion.png";
bulletImage.src = "assets/img/bullet.png";
powerupImage.src = "assets/img/hourglass.png";

// define audio files
const playerFire = new Audio("assets/sounds/playerShoot.wav");
const playerHit = new Audio("assets/sounds/playerHit.wav");
const enemyHit = new Audio("assets/sounds/enemyHit.wav");
const powerupSound = new Audio("assets/sounds/powerup.wav");
const BGM = new Audio("assets/sounds/megaman2banger.mp3");
const gameOverMusic = new Audio("assets/sounds/gameOver.wav");

// volume is a range between 0 and 1
// some of these wavs are way too damn loud
gameOverMusic.volume = 0.5;
playerHit.volume = 0.2;
enemyHit.volume = 0.2;
BGM.volume = 0.1;

let player = new GameObject(playerImage, canvas.width / 2 - 58.5, 600, PLAYER_WIDTH, PLAYER_HEIGHT);
let enemy1 = new GameObject(enemy1Image, canvas.width / 4.2 - 58.5, 100, PLAYER_WIDTH, PLAYER_HEIGHT);
let enemy2 = new GameObject(enemy2Image, canvas.width / 1.2 - 58.5, 100, ENEMY2_WIDTH, PLAYER_HEIGHT);

let imageHeight = 0; // the height of the background; used for scrolling two instances of the background 
let scrollSpeed = 5.5;  // speed at which the background scrolls
let playerMoveSpeed = 8;    // distance in px the player moves on keypress
let enemyBulletSpeed = 10;
enemy1.xSpeed = 3;    // initial xSpeed for enemies
enemy2.xSpeed = 3;

// variables needed to implement slowdown/time travel mechanic
let originalScrollSpeed = scrollSpeed;
let originalEnemySpeed1 = enemy1.xSpeed;
let originalEnemySpeed2 = enemy2.xSpeed;

// Speeds when the powerup is activated
let slowedScrollSpeed = 2;
let slowedEnemySpeed1 = 1;
let slowedEnemySpeed2 = 1;
let slowedBulletSpeed = 4;

// variables needed for the distance travelled and player health bar
let barWidth = 120;
let barHeight = 30;
let max = 100;
let val = 100;

// track distance travelled by the player
// initialize here and call function first when the game loads
let distance = 0;
showDistance();

function drawHealth() 
{
    context.font = "30px Georgia";
    context.fillStyle = "black";
    context.fillText("Health:", 5, 30);
    //draw background of the health bar first
    context.fillStyle = "#000000";
    context.fillRect(110, 8, barWidth, barHeight);

    //draw the fill of the health bar on top of this background
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(val / max, 0), 1);
    context.fillRect(110, 8, fillVal * barWidth, barHeight);
}

function GameObject(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this. x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}


let lastEnemyBulletTime = 0;
const enemyBulletInterval = 250; // enemies fire a bullet every second


// bullet object declaration here
let bullet = 
{
    image: bulletImage,
    speed: 5,
    width: 20,
    height: 20,
    list: [],
};

let enemyBullet =
{
    image: bulletImage,
    speed: enemyBulletSpeed,
    width: 20,
    height: 20,
    list: [],
};

// powerup declaration here
let powerup = 
{
    active: false,
    x: 0,
    y: 0,
    width: 50,
    height: 50,
};


function GamerInput(input)
{
    this.action = input;
}

let gamerInput = new GamerInput("None");

function input(event)
{
    console.log("Event type: " + event.type);

    if (event.type === "keydown") 
    {
        switch (event.key) 
        {
            case "w" :
                gamerInput = new GamerInput("w");
                break;
            case "a" :
                gamerInput = new GamerInput("a");
                break;
            case "s" :
                gamerInput = new GamerInput("s");
                break;
            case "d" :
                gamerInput = new GamerInput("d");
                break;
            case ' ' :
                if (gamerInput.action !== ' ')
                {
                    gamerInput = new GamerInput(' ');
                    fireBullet();
                    playerFire.play();
                }
                break;
            default:
                gamerInput = new GamerInput("None");
        }
    }
    else 
    {
        gamerInput = new GamerInput("None");
    }
}

function update()
{

    if (gamerInput.action === "w") 
    {
        // prevent the player from moving above the enemy sprites
        let upperLimit = Math.max(enemy1.y + enemy1.height, enemy2.y + enemy2.height);

        // also accounts for "ceiling" boundary checking since the top of the canvas can never be reached by the player
        if (player.y >= upperLimit) 
        {
            console.log("Moving Up");
            player.y -= playerMoveSpeed;
        }
    }

    if (gamerInput.action === "a") 
    {
        if (player.x >= 0)
        {
            console.log("Moving Left");
            player.x -= playerMoveSpeed;
        }
    }

    if (gamerInput.action === "s") 
    {
        if (player.y < canvas.height - player.height)
        {
            console.log("Moving Down");
            player.y += playerMoveSpeed;
        }
    }

    if (gamerInput.action === "d") 
    {
        if (player.x < canvas.width - player.width)
        {
            console.log("Moving Right");
        player.x += playerMoveSpeed;
        }
        
    }

    if (!isGameOver) // prevent the distance counter from increasing on the Game Over screen
    {
        distance++;
    }

    moveEnemies();
    enemyFireBullet(enemy1);
    enemyFireBullet(enemy2);
    updateEnemyBullets();
    checkCollisions();

    for (let i = 0; i < bullet.list.length; i++)
    {
        bullet.list[i].y -= bullet.speed; //move bullets that are added to the array upwards
        if (bullet.list[i].y < 0)
        {
            bullet.list.splice(i, 1); // bullets that hit the boundary are removed
            i--;
        }
    }
}

function showDistance() // simple function to show the distance that is incremented in update on screen/canvas
{
    context.font = "30px Georgia";
    context.fillStyle = "black";
    context.fillText("Distance:     " + distance, 400, 30);
}

function drawGameOver() 
{
    context.font = "25px Arial";
    context.fillStyle = "red";
    context.fillText("Game Over!", canvas.width / 2 - 150, canvas.height / 2);
    context.fillText("Distance Travelled: " + distance + "ft", canvas.width / 2 - 150, canvas.height / 2 + 25);
    context.fillText("Refresh the page to try again (F5)", canvas.width / 2 - 150, canvas.height / 2 + 50);
    // Pause all the audio elements
    playerFire.pause();
    playerHit.pause();
    enemyHit.pause();
    powerupSound.pause();
    BGM.pause();
    gameOverMusic.loop = false;
    gameOverMusic.play();
}

function getRandomPosition() 
{
    // Calculate the range for the powerup's position
    const minX = player.x + player.width / 2; // Left boundary based on player's position
    const maxX = canvas.width - powerup.width - enemy2.width / 2; // Right boundary based on canvas width and enemy's width

    // Calculate the vertical range below the enemies and above the player
    const minY = Math.max(enemy1.y + enemy1.height, enemy2.y + enemy2.height); // Minimum Y below the enemies
    const maxY = player.y - powerup.height; // Maximum Y above the player

    // Generate random X and Y positions within the specified ranges
    let randomX = Math.random() * (maxX - minX) + minX;
    let randomY = Math.random() * (maxY - minY) + minY;

    // Ensure the powerup remains within the canvas boundaries
    randomX = Math.min(Math.max(randomX, 0), canvas.width - powerup.width);
    randomY = Math.min(Math.max(randomY, 0), canvas.height - powerup.height);

    return { x: randomX, y: randomY };
}

function activatePowerup()
{
    powerup.active = true;
    powerupSound.play();
    const position = getRandomPosition();
    powerup.x = position.x;
    powerup.y = position.y;

    setTimeout(activatePowerup, 15000);
}

setTimeout(activatePowerup, 10000);

function fireBullet() // add a bullet to the array, give it coordinates to start moving from
{
    let bulletX = player.x + player.width / 2 - bullet.width / 2;
    let bulletY = player.y;
    bullet.list.push({x: bulletX, y: bulletY });
}

function enemyFireBullet()
{
    const currentTime = Date.now();

    // Check the time elapsed since the last bullet was fired by the enemy
    if (currentTime - lastEnemyBulletTime > enemyBulletInterval) 
    {
        enemyBullet.list.push({ x: enemy1.x + enemy1.width / 2 - enemyBullet.width / 2, y: enemy1.y });
        enemyBullet.list.push({ x: enemy2.x + enemy2.width / 2 - enemyBullet.width / 2, y: enemy2.y });
        lastEnemyBulletTime = currentTime; // Update the last bullet firing time
    }
}

function updateEnemyBullets() 
{
    for (let i = 0; i < enemyBullet.list.length; i++) 
    {
        enemyBullet.list[i].y += enemyBullet.speed; // Update y-position for bullet movement

        // Check if bullet is out of bounds (e.g., beyond canvas height)
        if (enemyBullet.list[i].y > canvas.height) {
            enemyBullet.list.splice(i, 1); // Remove bullet if it's out of bounds
            i--;
        }
    }
}

function moveEnemies() 
{
    // Move enemy1
    enemy1.x += enemy1.xSpeed; // Adjust the speed as needed

    // Reverse direction if enemy1 hits canvas boundaries
    if (enemy1.x <= 0)
    {
        enemy1.x = 0; // Reset to the left canvas boundary
        enemy1.xSpeed *= -1; // Reverse direction
    } 
    else if (enemy1.x + PLAYER_WIDTH >= canvas.width) 
    {
        enemy1.x = canvas.width - PLAYER_WIDTH; // Reset to the right canvas boundary
        enemy1.xSpeed *= -1; // Reverse direction
    }

    // Move enemy2
    enemy2.x += enemy2.xSpeed; // Move according to the xSpeed
    
    // Reverse direction if enemy2 hits canvas boundaries
    if (enemy2.x <= 0) 
    {
        enemy2.x = 0; // Reset to the left canvas boundary
        enemy2.xSpeed *= -1; // Reverse direction
    }
    else if (enemy2.x + PLAYER_WIDTH >= canvas.width) 
    {
        enemy2.x = canvas.width - PLAYER_WIDTH; // Reset to the right canvas boundary
        enemy2.xSpeed *= -1; // Reverse direction
    }

    // Check collision between enemies
    if (enemy1.x < enemy2.x + PLAYER_WIDTH && enemy1.x + PLAYER_WIDTH > enemy2.x)
     {
        // Handle collision by adjusting enemy positions (if required)
        enemy1.xSpeed *= -1;
        enemy2.xSpeed *= -1;
    }
}

function checkCollisions() 
{
    // check collision between enemies and player's bullets
    for (let i = 0; i < bullet.list.length; i++) 
    {
        let playerBullet = bullet.list[i];

        // Check collision with enemy1
        if (
            playerBullet.x < enemy1.x + enemy1.width &&
            playerBullet.x + bullet.width > enemy1.x &&
            playerBullet.y < enemy1.y + enemy1.height &&
            playerBullet.y + bullet.height > enemy1.y
        ) {
            // Play the sound for enemy hit
            enemyHit.play();
            enemy1HitCount++;
            if (enemy1HitCount >= 5)
            {
                despawnEnemy1();
            }

            // Remove the bullet that hit the enemy
            bullet.list.splice(i, 1);
            i--; // Decrement the index as the array length has decreased
        }

        // Check collision with enemy2
        if (
            playerBullet.x < enemy2.x + enemy2.width &&
            playerBullet.x + bullet.width > enemy2.x &&
            playerBullet.y < enemy2.y + enemy2.height &&
            playerBullet.y + bullet.height > enemy2.y
        ) {
            // Play the sound for enemy hit
            enemyHit.play();
            enemy2HitCount++;
            if (enemy2HitCount >= 5)
            {
                despawnEnemy2();
            }

            // Remove the bullet that hit the enemy
            bullet.list.splice(i, 1);
            i--; // Decrement the index as the array length has decreased
        }
    }



    // Check collision between player and enemy bullets
    for (let i = 0; i < enemyBullet.list.length; i++) 
    {
        let bullet = enemyBullet.list[i];

        // Check if the bullet hits the player
        if (
            bullet.x < player.x + player.width &&
            bullet.x + enemyBullet.width > player.x &&
            bullet.y < player.y + player.height &&
            bullet.y + enemyBullet.height > player.y
        ) {
            // Reduce player's health (modify this according to your health logic)
            playerHit.play();
            val -= 5;

            // Remove the bullet that hit the player
            enemyBullet.list.splice(i, 1);
            i--; // Decrement the index as the array length has decreased
        }
    }

    // Check collision between player and powerup
    if (
        player.x < powerup.x + powerup.width &&
        player.x + player.width > powerup.x &&
        player.y < powerup.y + powerup.height &&
        player.y + player.height > powerup.y
    ) {
        // Player collided with powerup
        powerup.active = false; // Hide the powerup

        activatePowerupEffects();
    }
}

function activatePowerupEffects() 
{
    powerupSound.play();

    // Apply slowdown to speeds
    scrollSpeed = slowedScrollSpeed;
    enemy1.xSpeed = slowedEnemySpeed1;
    enemy2.xSpeed = slowedEnemySpeed2;
    enemyBullet.speed = slowedBulletSpeed;

    val = Math.min(max, val + healthRegeneration); // Increase health, ensuring it doesn't exceed the maximum value

    // Set a timeout to revert speeds to normal after 10 seconds
    setTimeout(() => 
    {
        scrollSpeed = originalScrollSpeed;
        enemy1.xSpeed = originalEnemySpeed1;
        enemy2.xSpeed = originalEnemySpeed2;
        enemyBullet.speed = enemyBulletSpeed;

    }, 10000); 
}

function despawnEnemy1()
{
    enemyBullet.list = [];

    enemy1.drawDisabled = true;

    setTimeout(() => {
        // Re-enable drawing after 5 seconds
        enemy1.drawDisabled = false;
        enemy1HitCount = 0; // Reset hit count
    }, 5000); // 5 seconds
}

function despawnEnemy2()
{
    enemyBullet.list = [];
    
    enemy2.drawDisabled = true;

    setTimeout(() => {
        // Re-enable drawing after 5 seconds
        enemy2.drawDisabled = false;
        enemy2HitCount = 0; // Reset hit count
    }, 5000); // 5 seconds
}



function animate()
{
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(background, 0, imageHeight);
    context.drawImage(background, 0, imageHeight - canvas.height);
    context.drawImage(player.spritesheet, 0, 0, PLAYER_WIDTH, PLAYER_HEIGHT, player.x, player.y, PLAYER_WIDTH, PLAYER_HEIGHT);

    if (!enemy1.drawDisabled)
    {
        context.save(); // Save the current canvas state
        context.translate(enemy1.x + ENEMY2_WIDTH / 2, enemy1.y + PLAYER_HEIGHT / 2); // Set the rotation point to the center of the enemy image
        context.rotate(Math.PI); // Rotate 180 degrees (equivalent to PI radians)
        context.drawImage(enemy1.spritesheet, ENEMY2_WIDTH, PLAYER_HEIGHT, ENEMY2_WIDTH, PLAYER_HEIGHT, -ENEMY2_WIDTH / 2, -PLAYER_HEIGHT / 2, ENEMY2_WIDTH, PLAYER_HEIGHT); // Draw the rotated enemy image
        context.restore(); // Restore the canvas state to prevent further transformations affecting other drawings
    }
    
    if (!enemy2.drawDisabled)
    {
        context.save(); // Save the current canvas state
        context.translate(enemy2.x + PLAYER_WIDTH / 2, enemy2.y + PLAYER_HEIGHT / 2); // Set the rotation point to the center of the enemy image
        context.rotate(Math.PI); // Rotate 180 degrees (equivalent to PI radians)
        context.drawImage(enemy2.spritesheet, PLAYER_WIDTH * 2, PLAYER_HEIGHT * 2, PLAYER_WIDTH, PLAYER_HEIGHT, -PLAYER_WIDTH / 2, -PLAYER_HEIGHT / 2, PLAYER_WIDTH, PLAYER_HEIGHT); // Draw the rotated enemy image
        context.restore(); // Restore the canvas state to prevent further transformations affecting other drawings
    }
   
    imageHeight += scrollSpeed; // "scroll" the background image downwards

    if (imageHeight > canvas.height)
    {
        imageHeight = 0; // reset the image to the first of two backgrounds to reset the scrolling
    }
}

function draw()
{
    // Draw a black background covering the entire canvas
    context.fillStyle = "black";
    context.fillRect(0, 0, canvas.width, canvas.height);

    // Check if the player's health is zero
    if (val <= 0) 
    {
        // If health is zero, stop the game and draw "Game Over" text
        isGameOver = true;
        drawGameOver();
        return; // Stop further execution of the draw function
    }
    //context.drawImage(player.spritesheet, player.x, player.y, player.width, player.height);
    context.clearRect(0, 0, canvas.width, canvas.height);
    animate();
    showDistance();
    drawHealth();
    if (powerup.active) {
        context.drawImage(powerupImage, powerup.x, powerup.y, powerup.width, powerup.height);
    }

    // draw the player's bullets stored in the array
    for (let i =0; i < bullet.list.length; i++)
    {
        context.drawImage(
            bullet.image,
            bullet.list[i].x,
            bullet.list[i].y,
            bullet.width,
            bullet.height
        );
    }

    for (let i = 0; i < enemyBullet.list.length; i++) 
    {
        let bullet = enemyBullet.list[i];

        // Save the current canvas state to prevent affecting other drawings
        context.save();

        // Translate the canvas to the bullet's position
        context.translate(bullet.x + enemyBullet.width / 2, bullet.y + enemyBullet.height / 2);

        // Rotate the canvas by 180 degrees (PI radians)
        context.rotate(Math.PI);

        // Draw the rotated enemy bullet
        context.drawImage(
            enemyBullet.image,
            -enemyBullet.width / 2,
            -enemyBullet.height / 2,
            enemyBullet.width,
            enemyBullet.height
        );

        // Restore the canvas state to prevent further transformations affecting other drawings
        context.restore();
    }
}

function gameLoop()
{
    update();
    draw();
    window.requestAnimationFrame(gameLoop);
}

// Function to play background music
function playBackgroundMusic() 
{
    BGM.play();
    BGM.onended = function() 
    {
        BGM.play(); // Restart the music when it ends
    };
}

// Event listener for user interaction (e.g., click)
document.addEventListener('click', function() 
{
    playBackgroundMusic(); // Play music when the user clicks anywhere on the page
});

window.requestAnimationFrame(gameLoop);
window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
